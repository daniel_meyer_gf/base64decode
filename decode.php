<?php
header('Content-Type: application/json');

switch (@$_POST['action']) {
    case 'decodeBase64':
        echo base64_decode($_POST['code']);
        break;
    case 'formatJSON':
        echo $_POST['code'];
        break;
    default:
        echo json_encode($_POST);
}
